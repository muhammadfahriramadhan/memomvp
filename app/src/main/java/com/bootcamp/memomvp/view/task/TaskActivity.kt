package com.bootcamp.memomvp.view.task

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bootcamp.memomvp.R
import com.bootcamp.memomvp.data.source.TaskRepository
import com.bootcamp.memomvp.data.source.TaskRepositoryInterface
import com.bootcamp.memomvp.util.Notification
import kotlinx.android.synthetic.main.activity_task.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream

class TaskActivity : AppCompatActivity(),TaskView {
    lateinit var taskRepositoryInterface : TaskRepositoryInterface
    lateinit var taskPresenter: TaskPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)
        taskRepositoryInterface = TaskRepository(this)
        taskPresenter = TaskPresenterImplement(this,taskRepositoryInterface)
        setSupportActionBar(toolbar)

    }


    override fun onSuccessInsert(result: String) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
        Notification.showNotif(this,result)
        edtxtTaskTitle.setText("")
        edtxDescription.setText("")


    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
       return when(item.itemId){
            R.id.menuSave ->{
                val title = edtxtTaskTitle.text.toString()
                val description = edtxDescription.text.toString()
                insertToJson(title,description,{
                    Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
                })
//                taskPresenter.insertTask(TaskEntity(null,title,description))
                true
            }
            else ->  return super.onOptionsItemSelected(item)
        }

    }

    private fun insertToJson(title: String, description: String,callback : (result : String) -> Unit) {
        val jsonMemo = JSONObject()
        val jsonData = JSONObject()
        val jsonArray = JSONArray()
        jsonMemo.put("title",title)
        jsonMemo.put("description",description)
        jsonArray.put(jsonMemo)
        jsonData.put("datas",jsonArray)


        saveDataToExternal(this,jsonData,callback)

    }

    private fun saveDataToExternal(context: Context, jsonData: JSONObject, callback: (result: String) -> Unit) {
        val filename = "memo.json"
        val file = File(context.filesDir,filename)
        var outputStream : FileOutputStream

        if (!file.exists()){
            file.mkdirs()
        }

        try {
            outputStream = FileOutputStream(file)
            outputStream.write(jsonData.toString().toByteArray())
            callback.invoke("Success Input Data")
            outputStream.close()
        }catch (e : Exception){
            callback.invoke(e.printStackTrace().toString())
        }



    }


}