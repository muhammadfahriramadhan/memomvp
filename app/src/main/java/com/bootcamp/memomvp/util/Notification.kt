package com.bootcamp.memomvp.util

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import com.bootcamp.memomvp.R

object Notification {

    fun showNotif(context: Context , message : String){
        val channelId = "com.bootcamp.memomvp"
        val notificationManager : NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,"memonotif",NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
        }

        val builder = NotificationCompat.Builder(context,channelId)
                     .setSmallIcon(R.mipmap.ic_launcher)
                     .setContentTitle("MemoMvp")
                     .setContentText(message)
                     .setPriority(NotificationCompat.PRIORITY_HIGH)
                     .setDefaults(NotificationCompat.DEFAULT_ALL)

        notificationManager.notify(1,builder.build())

    }
}