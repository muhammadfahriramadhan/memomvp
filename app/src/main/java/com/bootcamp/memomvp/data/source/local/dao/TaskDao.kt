package com.bootcamp.memomvp.data.source.local.dao

import androidx.room.Dao
import androidx.room.Insert
import com.bootcamp.memomvp.data.model.TaskEntity

@Dao
interface TaskDao {

    @Insert
    fun insertTask(taskEntity: TaskEntity) : Long

}