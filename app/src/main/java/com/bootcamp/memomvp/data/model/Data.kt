package com.bootcamp.memomvp.data.model


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Data(
    @SerializedName("description")
    val description: String?,
    @SerializedName("title")
    val title: String?
)