package com.bootcamp.memomvp.data.source

import com.bootcamp.memomvp.data.model.TaskEntity

interface TaskRepositoryInterface {

    fun insertTask(taskEntity: TaskEntity) : Long?
}