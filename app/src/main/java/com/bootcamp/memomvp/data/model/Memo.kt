package com.bootcamp.memomvp.data.model


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Memo(
    @SerializedName("datas")
    val datas: List<Data>?
)